#!/bin/bash

URL='localhost:3000/'

for method in GET POST PUT DELETE; do
    echo "** CALLING $URL with $method **"
    curl -X $method $URL; echo
done
