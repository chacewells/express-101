const express = require('express');
// an 'app' is the express function (createApplication inside express module) invoked and is an Express application
const app = express();

// all is a method, and it takes 2 args:
// 1. route
// callback to run if route is requested
app.all('*', (req, res) => {
    // express handles the basic headers (status code, mime-type, etc.)! awesome!
    res.send(`<h1>This is the home page</h1>`);
    // express handles the end
});

app.listen('3000');
console.log('the server is listening on port 3000...');
