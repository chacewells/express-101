const express = require('express');
const app = express();

// app object has a few methods:
// 1. get
// 2. post
// 3. delete
// 4. put

// app.all('/', (req, res) => {
//     res.send(`<h1>Welcome to the home page!</h1>`);
// });

app.get('/', (req, res) => {
     res.send(`<h1>Welcome to the home GET page!</h1>`);
});

app.post('/', (req, res) => {
     res.send(`<h1>Welcome to the home POST page!</h1>`);
});

app.put('/', (req, res) => {
     res.send(`<h1>Welcome to the home PUT page!</h1>`);
});

app.delete('/', (req, res) => {
     res.send(`<h1>Welcome to the home DELETE page!</h1>`);
});

app.listen(3000);
console.log('the server is listening on port 3000...');
